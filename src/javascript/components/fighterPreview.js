import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const reversed = position === 'right' ? 'reversed':''
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName} fixed-size`,
  });

  try {
    const {name,attack,health,defense,source} = fighter
    const imgPrev = createFighterImage(fighter)
    const attribut = imgPrev.getAttribute('class')

    imgPrev.setAttribute("class", `${reversed} ${attribut}`)

    const decript = createElement({
      tagName: 'div',
      className: `decription`,
    });
    decript.innerText =  `${name} \nATTACK: ${attack} \n HEALTH: ${health} \nDEFENSE: ${defense}`
    fighterElement.append(imgPrev);
    fighterElement.append(decript);

  }catch (e) {
    console.error();
  }


  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}
