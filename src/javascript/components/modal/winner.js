import { showModal } from './modal';
import App from '../../app';
import { createElement } from '../../helpers/domHelper';
import { createFighterImage } from '../fighterPreview';

export function showWinnerModal(fighter) {
  const {name} = fighter
  const winner = `WINNER ${name}!!!`
  const body = createElement({
    tagName: 'div',
    className: `modal-body`,
  });
  const fighterElement = createElement({ tagName: 'div', className: 'fighters___fighter' });
  const imageElement = createFighterImage(fighter)
  fighterElement.append(imageElement);
  showModal({title:winner,bodyElement:fighterElement,onClose})
}

function onClose(){

  new App()
}
