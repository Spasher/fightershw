import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  firstFighter.curentHealth = firstFighter.health
  secondFighter.curentHealth = secondFighter.health
  return new Promise((resolve,reject) => {
    runOnKeys(
      ()=> {
        if(makeHit(firstFighter,secondFighter,'right',false)){
          const winner = firstFighter.curentHealth >= secondFighter.curentHealth ? firstFighter : secondFighter
          resolve(winner)
        }
      },
      controls.PlayerOneAttack
    );runOnKeys(
      ()=> {
        if(makeHit(firstFighter,secondFighter,'left',false)){
          const winner = firstFighter.curentHealth >= secondFighter.curentHealth ? firstFighter : secondFighter
          resolve(winner)
        }
      },
      controls.PlayerTwoAttack
    );
    runOnKeys(
      ()=> {
        if(makeHit(firstFighter,secondFighter,'right',true)){
          const winner = firstFighter.curentHealth >= secondFighter.curentHealth ? firstFighter : secondFighter
          resolve(winner)
        }
      },
      controls.PlayerOneAttack,
      controls.PlayerTwoBlock
    );
    runOnKeys(
      ()=> {
        if(makeHit(firstFighter,secondFighter,'left',true)){
          const winner = firstFighter.curentHealth >= secondFighter.curentHealth ? firstFighter : secondFighter
          resolve(winner)}
      },
      controls.PlayerTwoAttack,
      controls.PlayerOneBlock
    );
    runOnKeys(
      () => {
        if(makeHit(firstFighter,secondFighter,'left')){
          const winner = firstFighter.curentHealth >= secondFighter.curentHealth ? firstFighter : secondFighter
          resolve(winner)
        }
      },
      "KeyU",
      "KeyI",
      "KeyO"
    );
    runOnKeys(
      ()=> {
        if(makeHit(firstFighter,secondFighter,'right')){
          const winner = firstFighter.curentHealth >= secondFighter.curentHealth ? firstFighter : secondFighter
          resolve(winner)
        }
      },
      "KeyQ",
      "KeyW",
      "KeyE"
    );

  });

}
function makeHit(firstFighter,secondFighter,position,blocked){
  if(position === 'left'){
    const temp = secondFighter
    secondFighter = firstFighter
    firstFighter = temp
  }
  let damage = 0
  if(blocked){
     damage = getDamage(firstFighter,secondFighter)
  }else{
     damage = getHitPower(firstFighter,secondFighter)
  }
  const healthBar =  document.getElementById(`${position}-fighter-indicator`);
  secondFighter.curentHealth -= damage
  console.log(secondFighter.curentHealth)
  const res = secondFighter.curentHealth / secondFighter.health

  healthBar.style.width=`${res*100}%`
  if(firstFighter.curentHealth <= 0 || secondFighter.curentHealth <= 0){
  return true
  }else{
    return false
  }

}


function runOnKeys(func, ...codes) {
  let pressed = new Set();

  document.addEventListener('keydown', function(event) {
    pressed.add(event.code);

    for (let code of codes) {
      if (!pressed.has(code)) {
        return;
      }
    }
    pressed.clear();

    func();
  });

  document.addEventListener('keyup', function(event) {
    pressed.delete(event.code);
  });

}
export function getCriticalPower(fighter) {
  const {attack} = fighter
  const hit = attack * 2;
  return hit
}
export function getDamage(attacker, defender) {
   const damage = getHitPower(attacker) - getBlockPower(defender)
   return damage <= 0 ? 0 : damage;
}

export function getHitPower(fighter) {
   const {attack} = fighter
   const hit = attack * (Math.random() * (2 - 1) + 1);
   return hit
}

export function getBlockPower(fighter) {
  const {defense} = fighter
  const block = defense * (Math.random() * (2 - 1) + 1);
  return block
}
